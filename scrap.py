import requests
from bs4 import BeautifulSoup

listeTag = ["love", "inspirational", "life", "humor", "books", "reading"]
print(f"Voici une liste de tag: \n {listeTag}")

choice = input("Veuillez entrer un tag: ").lower()

if choice in listeTag :
    url = "http://quotes.toscrape.com/tag/"+ choice + "/" 

    response = requests.get(url)

    html = BeautifulSoup(response.content, "html.parser")

    result = html.find_all("div",{"class": "quote"})

    nombreQ = 5 
    print()
    
    if result:  
            for i,q in enumerate(result[:nombreQ],1):
                text = q.find("span", {"class": "text"}).text
                author = q.find("small", {"class": "author"}).text
                print(f"{text}\n   - {author}\n")
                print()

